# DSFall2015 Homework 1
# Server Code
import sys
import socket
import os
import time
import threading
import select

QUAD_AXE = 6789
MAX_QUEUED_CONS = 10

IN_BUF_SIZE = 8192

bEngineReady = False
engine_sock = None
players = {}
messages= []
messagesLock = threading.Condition()

def init_server():

    # Alloc TCP server sock and return it
    server_sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    server_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_sock.bind(("0.0.0.0",QUAD_AXE))
    # we will accept clients one by one so the client queue should be long enough
    server_sock.listen(MAX_QUEUED_CONS)
    print 'Server initialized on "0.0.0.0:%d" at: %s' % (QUAD_AXE, time.asctime())
    return server_sock

def engine_recv(topic, uid):
    """
    Reads from the message queue the message with the specified topic and user ID
    IIf none of the messages int he queue match the topic and id then returns None

    @param topic: The topic of interest
    @type topic: string
    @param uid: User ID
    @return: if topic is /addPlayerResponse (getting user ID) then returns user ID
             otherwise returns the message from the engine that corresponds to the topic and id specified
    
    """
    ret = None
    messagesLock.acquire()
    for resp in messages:
        words = resp.split(':', 3)
        if topic == '/addPlayerResponse':
            if words[0] == topic:
                ret = words[1]
                messages.remove(resp)
                break
        else:
            if words[0] == topic and words[1] == uid:
                ret = resp
                messages.remove(resp)
                break
    messagesLock.notify()
    messagesLock.release()
    return ret

def player_loop(sock, name):
    """
    This is the loop for thread that handles players, it adds the player to the field,
    waits for movement messages for 0.5 seconds and sends them to engine. Also every
    0.5 seconds it asks for the player Field of vision, score and ingame status and
    sends the information to players

    @param sock: player socket
    @param name: player name
    """
    global bEngineReady
    global engine_sock
    global players
    
    resp = None
    if not bEngineReady:
        sock.send('/noEngine')
    while not bEngineReady:
        #wait for engine to be connected
        i = 1
    if bEngineReady:
        engine_sock.send('/addPlayer:' + name)
        while resp == None:
            resp = engine_recv('/addPlayerResponse', 0)
        UUID = resp
        sock.send(UUID)
        players[UUID] = sock
        while(1):
            FOV = None
            score = None
            ingame = None
            skip = False
            ready = select.select([sock], [], [], 0.5)
            if ready[0]:
                data = sock.recv(IN_BUF_SIZE)
                words = data.split(':', 3)
                if words[0] == '/move':
                    engine_sock.send(words[0] + ':' + UUID + ':' + words[1])
                if words[0] == '/join':
                    engine_sock.send(words[0] + ':' + UUID + ':' + words[1])
                    print words[0] + ':' + UUID + ':' + words[1]
                    skip = True
            if not skip:
                engine_sock.send('/getFOV:' + UUID)
                while FOV == None:
                    FOV = engine_recv('/getFOVResponse', UUID)
                sock.send(FOV + ':')
                engine_sock.send('/getScore:' + UUID)
                while score == None:
                    score = engine_recv('/getScoreResponse', UUID)
                sock.send(score + ':')
                engine_sock.send('/getIfInGame:' + UUID)
                while ingame == None:
                    ingame = engine_recv('/getIfInGameResponse', UUID)
                sock.send(ingame)   
    else:
        sock.send('/noEngine')

def engine_loop(sock):
    """
    This is the loop for engine thread. It reads from the engine socket and
    puts all the messages in the message queue

    @param sock: engine socket

    """
    while 1:
        m = sock.recv(IN_BUF_SIZE)
        messagesLock.acquire()
        messages.append(m)
        messagesLock.notify()
        messagesLock.release()
            

def who_is_this(sock, w, h):
    """
    This is the function that sees wheather a player or an engine has connected
    the server

    @param sock: client socket
    @param w: game field width
    @param h: game field height
    """
    global bEngineReady
    global engine_sock
    data = sock.recv(IN_BUF_SIZE)
    words = data.split(':', 2)
    if words[0] == '/player':
        #player connected
        player_loop(sock, words[1])
    elif words[0] == '/engine':
        #engine connected
        if not bEngineReady:
            engine_sock = sock
            engine_sock.send('/init:' + w + ':' + h)
            bEngineReady = True
            engine_loop(engine_sock)

def create_thread(sock, w, h):
    """
    Here a separate thread is created for each connected client
    """
    args = {'sock':sock}
    name = 'Thread for: %s' % ':'.join(map(str, sock.getpeername()))
    def inner(x):
        who_is_this(sock, w, h)
    t = threading.Thread(target =inner, name = name, args = args)
    t.start()

def listen_user():
    """
    This is the loop for the thread that listens for keyboard interrupt
    It is not working for windows computers because ctrl-C in windows doesn't
    generate a KeyboardInterrupt while the main thread executes socket.accept()
    it should be working on linux, but there has been no way to test it

    On KeyboardInterrupt all connected players are notified of the shutdown
    """
    while 1:
        try:
            i = 1
        except KeyboardInterrupt:
            for s in players:
                s.send('/shutDown')
            sys.exit()

def server_loop(server_sock, w, h):
    """
    This is the main server loop. The server waits for clients to connect and
    creates a thread for each of them
    """
    while 1:
        client_sock, addr= server_sock.accept()
        print "Client connected from: %s at %s" % (':'.join(map(str,addr)),time.asctime())
        create_thread(client_sock, w, h)

def eat_args():
    if len(sys.argv) <= 2:
        sys.stderr.write('Usage: %s <width> <height>\n' % sys.argv[0])
        sys.exit(1)
    return (sys.argv[1], sys.argv[2])
if __name__ == '__main__':
    (w, h) = eat_args()
    server_sock = init_server()
    t = threading.Thread(target = listen_user, name = 'user Thread')
    t.start()
    server_loop(server_sock, w, h)

# Engine Code

import sys
import socket
import time
import threading
from enum import Enum
import random

QUAD_AXE = 6789
IN_BUF_SIZE = 8192

gameField = None
gameFieldWidth = 0
gameFieldHeight = 0
gameFieldLock = threading.Condition()

players = {}
playersLock = threading.Condition()

UUID = 0

class Character(Enum):
	"""
    Class for having enums for the characters used in the game
    """
	NOBODY = 0
	SPECTATOR = 1
	FROG = 2
	FLY = 3

class Player(object):
	"""
    Class for players in the game
    """
	def __init__(self,name):
		"""
		Initializes players
		@type  name: string 
		@param name: name of the player
		"""
		self.UUID = str(generateUUID())
		self.name = name
		self.character = Character.SPECTATOR
		self.earnedPoints = 0
		self.timestampOfLastScore = self.getTimeStamp()
		self.timestampWhenJoined = self.getTimeStamp()
		self.playerNextMove = None
		self.posX = None
		self.posY = None
	def getTimeStamp(self):
		"""
		Returns current timestamp
		@rtype:   string
		@return:  string of current float timestamp
		"""
		return '%f' % time.time()
	def getPlayerUUID(self):
		"""
		Returns player UUID
		@rtype:   int
		@return:  Unique id of a player
		"""
		return self.UUID
	def joinGame(self, character):
		"""
		Initializes parameters necessary to play game
		@type  character: Character
		@param character: character of the player
		"""
		self.character = character
		self.timestampOfLastScore = self.getTimeStamp()
		self.timestampWhenJoined = self.getTimeStamp()
	def registerNextMove(self, move):
		"""
		Registers the next movement of the player
		@type  move: string
		@param move: string of next move
		"""
		self.playerNextMove = move
	def getXPos(self):
		"""
		Returns x position of player in the gamefield
		@rtype:   int
		@return:  x-position of the player
		"""
		return self.posX
	def getYPos(self):
		"""
		Returns y position of player in the gamefield
		@rtype:   int
		@return:  y-position of the player
		"""
		return self.posY
	def getScore(self):
		"""
		Returns player's score
		@rtype:   int
		@return:  player score
		"""
		return self.earnedPoints
	def setScore(self, points):
		"""
		Sets player's score
		@type  points: int
		@param points: player's points
		"""
		self.earnedPoints = points
	def getCharacter(self):
		"""
		Returns player's character
		@rtype:   Character
		@return:  player's character
		"""
		return self.character
	def	setXPos(self,xPos):
		"""
		Sets player's x-position
		@type  points: int
		@param points: player's x-position
		"""
		self.posX = xPos
	def	setYPos(self,yPos):
		"""
		Sets player's y-position
		@type  points: int
		@param points: player's y-position
		"""
		self.posY = yPos
	def getNextMove(self):
		"""
		Returns player's next move
		@rtype:   string
		@return:  player's next move
		"""
		return self.playerNextMove
	def setTimeStampOfLastScore(self):
		"""
		Sets player's last scoring timestamp
		"""
		self.timestampOfLastScore = self.getTimeStamp()
	def getTimeStampOfLastScore(self):
		"""
		Returns player's last scoring timestamp
		@rtype:   float
		@return:  player's last scoring timestamp
		"""
		return self.timestampOfLastScore
	def die(self):
		"""
		Parameters to set when player dies
		"""
		self.character = Character.SPECTATOR
		self.earnedPoints = 0
		self.posX = None
		self.posY = None
	def getTimeStampWhenJoined(self):
		"""
		Returns player's timestamp when joined
		@rtype:   float
		@return:  player's timestamp when joined
		"""
		return self.timestampWhenJoined
		
def generateUUID():
	"""
	Returns unique identifier for player
	@rtype:   int
	@return:  UUID for player
	"""
	global UUID
	UUID += 1
	return UUID
	
def checkIfPosOkay(xPos, yPos):
	"""
	Checks if position is okay for player placement
	@type  xPos: int
    @param xPos: x-position to check
	@type  yPos: int
    @param yPos: y-position to check
	@rtype:   boolean
	@return:  if position is not occupied
	"""
	ifOkay = False
	gameFieldLock.acquire()
	if gameField[yPos][xPos] == 0:
		ifOkay = True
	gameFieldLock.notify()
	gameFieldLock.release()
	return ifOkay
	
def generatePosition():
	"""
	generates position for player placement
	@rtype:   dict
	@return:  dictionary containing player x and y position
	"""
	xPos = None
	yPos = None
	ifOkayPosFound = False
	while not ifOkayPosFound:
		xPos = random.randint(0, gameFieldWidth-1)
		yPos = random.randint(0, gameFieldHeight-1)
		ifOkayPosFound = checkIfPosOkay(xPos, yPos)
	return {'xPos':xPos, 'yPos':yPos}

def eat_args():
	"""
	Returns host server address
	@rtype:   string
	@return: host server address
	"""
	if len(sys.argv) <= 1:
		sys.stderr.write('Usage: %s <host server>\n' % sys.argv[0])
		sys.exit(1)
	return sys.argv[1]

def init_game_field(w, h):
	"""
	Initiates gamefield
	@type  w: int
    @param w: width of gamefield
	@type  h: int
    @param h: height of gamefield
	"""
	global gameField, gameFieldWidth, gameFieldHeight
	gameFieldWidth = w
	gameFieldHeight = h
	gameField = []
	for i in range (0, h):
		new = []
		for j in range (0, w):
			new.append(0)
		gameField.append(new)
	
def getCharacterAccordingToWord(word):
	"""
	Returns character enum according to input word
	@type  word: string
    @param word: character string representation
	@rtype:   Character
	@return: character enum representation
	"""
	if word == "Frog":
		return Character.FROG
	elif word == "Fly":
		return Character.FLY
	else: 
		return Character.SPECTATOR
	
def getMovementCoordinateAccordingToWord(word):
	"""
	Returns movement according to input word
	@type  word: string
    @param word: movement string representation
	@rtype:   int
	@return: movement int representation
	"""
	xMov = 0
	yMov = 0
	if word == "Up":
		yMov = -1
	elif word == "Down":
		yMov = 1
	elif word == "Right":
		xMov = 1
	elif word == "Left":
		xMov = -1
	elif word == "ShiftUp":
		yMov = -2
	elif word == "ShiftDown":
		yMov = 2
	elif word == "ShiftRight":
		xMov = 2
	elif word == "ShiftLeft":
		xMov = -2
	return {'xMov':xMov, 'yMov':yMov}
		
def checkIfPositionInField(xPos, yPos):
	"""
	Checks if position is in gamefield
	@type  xPos: int
    @param xPos: x-position to check
	@type  yPos: int
    @param yPos: y-position to check
	@rtype:   boolean
	@return: if position is part of the gamefield
	"""
	if gameFieldWidth > xPos and gameFieldHeight > yPos and xPos > -1 and yPos > -1:
		return True
	return False
	
	
def checkIfManhattanDistanceSmallerThanAllowed(playerPosX, playerPosY, checkPosX, checkPosY, playerCharacter):
	"""
	Checks if distance is smaller than allowed manhattan distance from the players position
	@type  playerPosX: int
    @param playerPosX: x-position player
	@type  playerPosY: int
    @param playerPosY: y-position player
	@type  checkPosX: int
    @param checkPosX: x-position to check
	@type  checkPosY: int
    @param checkPosY: y-position to check
	@type  playerCharacter: Character
    @param playerCharacter: player's character
	@rtype:   boolean
	@return: if in allowed manhattan distance according to character
	"""
	manhattanDistance = abs(playerPosX-checkPosX) + abs(playerPosY-checkPosY)
	if playerCharacter == Character.FLY:
		if manhattanDistance > 3:
			return False
		return True
	elif playerCharacter == Character.FROG:
		if manhattanDistance > 1:
			return False
		return True
		
def getFieldOfVisionMatrix(XPosOfPlayer, YPosOfPlayer, playerCharacter):
	"""
	Return field of vision of the player according to parameters
	@type  XPosOfPlayer: int
    @param XPosOfPlayer: x-position player
	@type  YPosOfPlayer: int
    @param YPosOfPlayer: y-position player
	@type  playerCharacter: Character
    @param playerCharacter: player's character
	@rtype:   2D array
	@return: Field of vision for the player
	"""
	global gameFieldWidth, gameFieldHeight
	returnMatrix = [[]]
	gameFieldLock.acquire()
	howBigIsField = 0
	howMuchToSubstractFromPos = 0
	if playerCharacter == Character.FROG:
		howBigIsField = 3
		howMuchToSubstractFromPos = 2
	elif playerCharacter == Character.FLY:
		howBigIsField = 5
		howMuchToSubstractFromPos = 3
	if playerCharacter != Character.SPECTATOR and YPosOfPlayer != None and XPosOfPlayer != None:
		returnMatrix = [[-1 for y in range(gameFieldWidth)] for x in range(gameFieldHeight)]
		yPosCheck = YPosOfPlayer-howMuchToSubstractFromPos
		for y in range(howBigIsField):
			yPosCheck += 1
			xPosCheck = XPosOfPlayer-howMuchToSubstractFromPos
			for x in range(howBigIsField):
				xPosCheck += 1
				if checkIfPositionInField(xPosCheck, yPosCheck) and checkIfManhattanDistanceSmallerThanAllowed(XPosOfPlayer,YPosOfPlayer,xPosCheck,yPosCheck,playerCharacter):
					if gameField[yPosCheck][xPosCheck] != 0:
						playersLock.acquire()
						if players[gameField[yPosCheck][xPosCheck]].getCharacter() == Character.FROG:
							returnMatrix[yPosCheck][xPosCheck] = 1
						elif players[gameField[yPosCheck][xPosCheck]].getCharacter() == Character.FLY:
							returnMatrix[yPosCheck][xPosCheck] = 2
						playersLock.notify()
						playersLock.release()
					else:
						returnMatrix[yPosCheck][xPosCheck] = 0
	elif playerCharacter == Character.SPECTATOR or YPosOfPlayer == None or XPosOfPlayer == None:
		returnMatrix = [[0 for y in range(gameFieldWidth)] for x in range(gameFieldHeight)]
		for y in range(gameFieldHeight):
			for x in range(gameFieldWidth):
				if gameField[y][x] != 0:
					playersLock.acquire()
					if players[gameField[y][x]].getCharacter() == Character.FROG:
						returnMatrix[y][x] = 1
					elif players[gameField[y][x]].getCharacter() == Character.FLY:
						returnMatrix[y][x] = 2
					playersLock.notify()
					playersLock.release()
	gameFieldLock.notify()
	gameFieldLock.release()
	return returnMatrix
	
def	addNewPlayersToTheField():
	"""
	Adds new players to the gamefield
	"""
	playersLock.acquire()
	for key in players:
		player = players[key]
		if player.getXPos() == None and player.getYPos() == None and player.getCharacter() != Character.SPECTATOR:
			generatedPlayerPositions = generatePosition()
			xPos = generatedPlayerPositions["xPos"]
			yPos = generatedPlayerPositions["yPos"]
			player.setXPos(xPos)
			player.setYPos(yPos)
			gameFieldLock.acquire()
			gameField[yPos][xPos] = player.getPlayerUUID()
			gameFieldLock.notify()
			gameFieldLock.release()
	playersLock.notify()
	playersLock.release()
	
def	whoIsInPosition(x, y):
	"""
	Return character in the gamefield if anybody is in there
	@type  x: int
    @param x: x-position 
	@type  y: int
    @param y: y-position 
	@rtype:   Character
	@return: Player's character in the position
	"""
	characterToReturn = Character.NOBODY
	gameFieldLock.acquire()
	playerIdOnField = gameField[y][x]
	if playerIdOnField != 0:
		playersLock.acquire()
		characterToReturn = players[playerIdOnField].getCharacter()
		playersLock.notify()
		playersLock.release()
	gameFieldLock.notify()
	gameFieldLock.release()
	return characterToReturn
	
def makeNextMove(player):
	"""
	Make player's next move, takes into account what changes need to be made on the gamefield and with scores
	@type  player: Player
    @param x: player who make the next move
	"""
	moveDictionary = getMovementCoordinateAccordingToWord(player.getNextMove())
	player.registerNextMove(None)
	moveX = player.getXPos() + moveDictionary["xMov"]
	moveY = player.getYPos() + moveDictionary["yMov"]
	currentCharacter = player.getCharacter()
	if checkIfPositionInField(moveX, moveY):
		characterAlreadyInPosition = whoIsInPosition(moveX, moveY)
		movementSuccessful = False
		wasterOtherPlayer = False
		addPointsToMeBecauseOtherEaten = False
		if characterAlreadyInPosition == Character.NOBODY:			
			movementSuccessful = True
		elif characterAlreadyInPosition == Character.FLY:
			if currentCharacter == Character.FROG:
				wasterOtherPlayer = True
				movementSuccessful = True
				addPointsToMeBecauseOtherEaten = True
		if wasterOtherPlayer == True:
			gameFieldLock.acquire()
			playerUUIDToBeWasted = gameField[moveY][moveX]
			playerToBeWasted = players[playerUUIDToBeWasted]
			playerToBeWasted.die()
			gameFieldLock.release()
		if movementSuccessful == True:
			gameFieldLock.acquire()
			gameField[player.getYPos()][player.getXPos()] = 0
			player.setXPos(moveX)
			player.setYPos(moveY)
			gameField[player.getYPos()][player.getXPos()] = player.getPlayerUUID()
			gameFieldLock.notify()
			gameFieldLock.release()		
		if addPointsToMeBecauseOtherEaten == True:
			player.setScore(player.getScore() + 1)
			player.setTimeStampOfLastScore()
			
def	processPlayerInputs():
	"""
	If players have a next move then forwards the action to the next method
	"""
	playersLock.acquire()
	for key in players:
		player = players[key]
		if player.getNextMove() != None and player.getCharacter() != Character.SPECTATOR:
			makeNextMove(player)
	playersLock.notify()
	playersLock.release()
	
def	detectSurvivingFlyesAndHungryFrogs():
	"""
	Checks if flyes have been alive long enough to earn points or if frog have been in hunger for so long that they must die
	"""
	playersLock.acquire()
	for key in players:
		player = players[key]
		if player.getCharacter() == Character.FROG:
			if float(player.getTimeStamp()) - float(player.getTimeStampOfLastScore()) > 120:
				gameFieldLock.acquire()
				gameField[player.getYPos()][player.getXPos()] = 0
				gameFieldLock.notify()
				gameFieldLock.release()
				player.die()
		elif player.getCharacter() == Character.FLY:
			if float(player.getTimeStamp()) - float(player.getTimeStampOfLastScore()) > 120:
				player.setScore(player.getScore() + 1)
				player.setTimeStampOfLastScore()
	playersLock.notify()
	playersLock.release()

def comm_thread(sock):
	"""
	Communication with the server goes on here, logic in here according to the assigment
	@type  sock: socket
    @param sock: server socket
	"""
	while True:
		data = sock.recv(IN_BUF_SIZE)
		words = data.split(':', 3)
		if words[0] == '/init':
			print "/init, width: " + words[1]  + ", height: " + words[2]
			width = words[1]
			height = words[2]
			init_game_field(int(width), int(height))
			print "gameField rows: " + str(len(gameField)) + ", columns: " + str(len(gameField[0]))
		elif words[0] == '/addPlayer':
			print "/addPlayer, playerName: " + words[1]
			playerName = words[1]
			newPlayer = Player(playerName)
			playersLock.acquire()
			players[newPlayer.getPlayerUUID()] = newPlayer
			print "Number of players: " + str(len(players))
			playersLock.notify()
			playersLock.release()
			response = "/addPlayerResponse:" + str(newPlayer.getPlayerUUID())
			sock.send(response)
		elif words[0] == '/join':
			print "/join, UUID: " + words[1]  + ", Character: " + words[2]
			playerUUID = words[1]
			characterWord = words[2]
			character = getCharacterAccordingToWord(characterWord)
			playersLock.acquire()
			players[playerUUID].joinGame(character)
			print "Player joined at: " + str(players[playerUUID].getTimeStampWhenJoined())
			playersLock.notify()
			playersLock.release()
			sock.send("/joinResponse")
		elif words[0] == '/move':
			print "/move, UUID: " + words[1]  + ", movement: " + words[2]
			playerUUID = words[1]
			movementWord = words[2]
			playersLock.acquire()
			players[playerUUID].registerNextMove(movementWord)
			print "Movement registered as: " + str(players[playerUUID].getNextMove())
			playersLock.notify()
			playersLock.release()
			sock.send("/moveResponse")
		elif words[0] == '/getFOV':
			print "/getFOV, UUID: " + words[1]
			playerUUID = words[1]
			playersLock.acquire()
			XPosOfPlayer = players[playerUUID].getXPos()
			YPosOfPlayer = players[playerUUID].getYPos()
			playerCharacter = players[playerUUID].getCharacter()
			playersLock.notify()
			playersLock.release()
			fieldOfVisionMatrix = getFieldOfVisionMatrix(XPosOfPlayer, YPosOfPlayer, playerCharacter)
			response = "/getFOVResponse:" + words[1]
			for x in range(gameFieldHeight):
				response += ":"
				response += ';'.join(str(e) for e in fieldOfVisionMatrix[x])
			response += ":/endOfFOVResponse"
			print "Response for FOV: " + response
			sock.send(response)
		elif words[0] == '/getScore':
			print "/getScore, UUID: " + words[1]
			playerUUID = words[1]
			playersLock.acquire()
			scoreOfPlayer = players[playerUUID].getScore()
			playersLock.notify()
			playersLock.release()
			response = "/getScoreResponse:" + words[1] + ':' + str(scoreOfPlayer)
			print "Response for score: " + response
			sock.send(response)
		elif words[0] == '/getIfInGame':
			print "/getIfInGame, UUID: " + words[1]
			playerUUID = words[1]
			playersLock.acquire()
			characterOfPlayer = players[playerUUID].getCharacter()
			playersLock.notify()
			playersLock.release()
			ifInGame = 0
			if characterOfPlayer == Character.FROG or characterOfPlayer == Character.FLY:
				ifInGame = 1
			response = "/getIfInGameResponse:" + playerUUID + ':' + str(ifInGame)
			print "Response for if in game: " + response
			sock.send(response)

def engine_worker(sock):
	"""
	Initiates server communication and works after every 500ms
	@type  sock: socket
    @param sock: server socket
	"""
	args = {'sock':sock}
	name = 'Comunication thread for: %s' % ':'.join(map(str, sock.getpeername()))
	def inner(x):
		comm_thread(sock)
	t = threading.Thread(target =inner, name = name, args = args)
	t.start()
	while True:
		if gameField != None:
			addNewPlayersToTheField()
			processPlayerInputs()
			detectSurvivingFlyesAndHungryFrogs()
		time.sleep(0.5)
		
if __name__ == '__main__':
	server_sock = eat_args()
	engine_sock = socket.create_connection((server_sock, QUAD_AXE))
	engine_sock.send('/engine')
	engine_worker(engine_sock)

# Engine Code

import sys
import socket
import time
import threading

QUAD_AXE = 6789
IN_BUF_SIZE = 8192


UUID = 0
def eat_args():
    if len(sys.argv) <= 1:
        sys.stderr.write('Usage: %s <host server>\n' % sys.argv[0])
        sys.exit(1)
    return sys.argv[1]

def init_game_field(w, h):
    i =1

def comm_loop(sock):
    global UUID
    while(1):
        data = sock.recv(IN_BUF_SIZE)
        words = data.split(':', 3)
        if words[0] == '/init':
            init_game_field(words[1], words[2])
        elif words[0] == '/addPlayer':
            #add new player
            sock.send(str(UUID))
            UUID = UUID + 1
        elif words[0] == '/join':
            print data
        elif words[0] == '/move':
            print data
        elif words[0] == '/getFOV':
            sock.send('FOV')
        elif words[0] == '/getScore':
            sock.send('score')


def comm_thread(sock):
    args = {'sock':sock}
    name = 'Comunication thread for: %s' % ':'.join(map(str, sock.getpeername()))
    def inner(x):
        comm_loop(sock)
    t = threading.Thread(target =inner, name = name, args = args)
    t.start()

if __name__ == '__main__':
    server_sock = eat_args()
    engine_sock = socket.create_connection((server_sock, QUAD_AXE))
    engine_sock.send('/engine')
    comm_thread(engine_sock)
    
    
    

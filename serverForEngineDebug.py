# DSFall2015 Homework 1
# Server Code
import sys
import socket
import os
import time
import threading
import select
from sys import stdin

QUAD_AXE = 6789
MAX_QUEUED_CONS = 10

IN_BUF_SIZE = 8192

bEngineReady = False
engine_sock = None
players = {}

def init_server():
    # Alloc TCP server sock and return it
    server_sock = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
    server_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_sock.bind(("0.0.0.0",QUAD_AXE))
    # we will accept clients one by one so the client queue should be long enough
    server_sock.listen(MAX_QUEUED_CONS)
    print 'Server initialized on "0.0.0.0:%d" at: %s' % (QUAD_AXE, time.asctime())
    return server_sock            

def who_is_this(sock, w, h):
    global bEngineReady
    global engine_sock
    data = sock.recv(IN_BUF_SIZE)
    words = data.split(':', 2)
    if words[0] == '/engine':
        #engine connected
        print "Engine connected"
        if not bEngineReady:
            engine_sock = sock
            engine_sock.send('/init:' + w + ':' + h)
            bEngineReady = True
            while(1):
                userinput = raw_input('')
                engine_sock.send(userinput)
                data = sock.recv(IN_BUF_SIZE)
                print data

def server_loop(server_sock, w, h):
    while 1:
        client_sock, addr= server_sock.accept()
        print "Client connected from: %s at %s" % (':'.join(map(str,addr)),time.asctime())
        who_is_this(client_sock, w, h)

def eat_args():
    if len(sys.argv) <= 2:
        sys.stderr.write('Usage: %s <width> <height>\n' % sys.argv[0])
        sys.exit(1)
    return (sys.argv[1], sys.argv[2])
if __name__ == '__main__':
    (w, h) = eat_args()
    server_sock = init_server()
    server_loop(server_sock, w, h)

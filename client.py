# Client Code

import sys
import socket
import time
import threading
import Tkinter
import os

QUAD_AXE = 6789
IN_BUF_SIZE = 8192

mode = 0

def eat_args():
    if len(sys.argv) <= 2:
        sys.stderr.write('Usage: %s <host server> <playername>\n' % sys.argv[0])
        sys.exit(1)
    return (sys.argv[1], sys.argv[2])

"""
These are the callback functions to handle user keyboard inputs and button clicks
"""
def bFrogCallback(sock):
    global mode
    if mode == 0:
        sock.send('/join:Frog')
        mode = 2
def bFlyCallback(sock):
    global mode
    if mode == 0:
        sock.send('/join:Fly')
        mode = 1
def UpCallback(sock, event):
    global mode
    if mode > 0:
        sock.send('/move:Up')
def DownCallback(sock, event):
    global mode
    if mode > 0:
        sock.send('/move:Down')
def LeftCallback(sock, event):
    global mode
    if mode > 0:
        sock.send('/move:Left')
def RightCallback(sock, event):
    global mode
    if mode > 0:
        sock.send('/move:Right')
def ShiftUpCallback(sock, event):
    global mode
    if mode > 1:
        sock.send('/move:ShiftUp')
def ShiftDownCallback(sock, event):
    global mode
    if mode > 1:
        sock.send('/move:ShiftDown')
def ShiftLeftCallback(sock, event):
    global mode
    if mode > 1:
        sock.send('/move:ShiftLeft')
def ShiftRightCallback(sock, event):
    global mode
    if mode > 1:
        sock.send('/move:ShiftRight')
    
              

def input_parser(sock):
    """
    Here a simple UI is created with two buttons for character selection and
    keyboard listeners for movement

    @param sock: client socket
    """
    master = Tkinter.Tk()
    def bFrogWrapper():
        bFrogCallback(sock)
    bFrog = Tkinter.Button(master, text="Frog", command = bFrogWrapper)
    
    def bFlyWrapper():
        bFlyCallback(sock)
        
    bFly = Tkinter.Button(master, text="Fly", command = bFlyWrapper)
    
    
    def UpWrapper(event):
        UpCallback(sock, event)
    master.bind("<Up>", UpWrapper)
    def DownWrapper(event):
        DownCallback(sock, event)
    master.bind("<Down>", DownWrapper)
    def LeftWrapper(event):
        LeftCallback(sock, event)
    master.bind("<Left>", LeftWrapper)
    def RightWrapper(event):
        RightCallback(sock, event)
    master.bind("<Right>", RightWrapper)

    def ShiftUpWrapper(event):
        ShiftUpCallback(sock, event)
    master.bind("<Shift-Up>", ShiftUpWrapper)
    def ShiftDownWrapper(event):
        ShiftDownCallback(sock, event)
    master.bind("<Shift-Down>", ShiftDownWrapper)
    def ShiftLeftWrapper(event):
        ShiftLeftCallback(sock, event)
    master.bind("<Shift-Left>", ShiftLeftWrapper)
    def ShiftRightWrapper(event):
        ShiftRightCallback(sock, event)
    master.bind("<Shift-Right>", ShiftRightWrapper)

    bFly.pack()
    bFrog.pack()
    Tkinter.mainloop()
    
def input_parser_thread(sock):
    """
    Here a thread is created for listening the user input

    @param sock: client socket
    """
    args = {'sock':sock}
    name = 'Thread for: %s' % ':'.join(map(str, sock.getpeername()))
    def inner(x):
        input_parser(sock)
    t = threading.Thread(target =inner, name = name, args = args)
    t.start()

def renderer(client_sock):
    """
    Here the messages from the server are received and the game field
    is rendered. Also when the player dies its mode is set to spectator and
    the player can rejoin the game

    @param client_sock: client socket
    """
    global mode
    score = 0
    ingame = 0
    while(1):
        data = client_sock.recv(IN_BUF_SIZE)
        words = data.split(':')
        if words[0] == '/getFOVResponse':
            FOV = words[2].replace(';', ' ').replace('-1', '*')
            if len(words) >= 3:
                i = 3
                while words[i] != '/endOfFOVResponse':
                    FOV += '\n' + words[i].replace(';', ' ').replace('-1', '*')
                    i += 1
                    if i >= len(words):
                        break
        elif words[0] == '/getScoreResponse':
            score = words[2]
            if len(words) >= 5:
                if words[5] != '0':
                    ingame = 1
                elif mode > 0 and ingame == 1:
                    mode = 0
                    ingame = 0
        elif words[0] == '/shutDown':
            print 'server has shut down unexpectedly, sorry'
            sys.exit()
        os.system(clearing)
        print 'current score: ' + str(score) + '\n\n'
        print FOV

if __name__ == '__main__':
    (server_sock, playername) = eat_args()
    client_sock = socket.create_connection((server_sock, QUAD_AXE))
    client_sock.send('/player:'+ playername)
    resp = '/noEngine'
    while resp == '/noEngine':
        resp = client_sock.recv(IN_BUF_SIZE)
    print resp
    if sys.platform == 'win32' or sys.platform == 'Windows':
        clearing = 'cls'
    else:
        clearing = 'clear'
    input_parser_thread(client_sock)
    renderer(client_sock)
    
        
